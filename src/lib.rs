pub mod error;
pub mod types;

pub use crate::error::{
    PopError,
    PopResult,
};
use crate::types::{
    SearchResults,
    StreamLocation,
};
use m3u8_rs::playlist::{
    MasterPlaylist,
    Playlist,
};
use reqwest::header::{
    ACCEPT,
    CONTENT_TYPE,
    ORIGIN,
};
use select::{
    document::Document,
    predicate::{
        And,
        Attr,
        Name,
        Not,
        Text,
    },
};
use serde_json::json;
use url::Url;

const SEARCH_URL: &str = "https://www.popcornflix.com/searchs";

pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        Client {
            client: reqwest::Client::new(),
        }
    }

    pub async fn search(&self, query: &str) -> PopResult<SearchResults> {
        let url = Url::parse_with_params(SEARCH_URL, &[("q", query)])?;
        let res = self.client.get(url).send().await?;
        let status = res.status();

        if !status.is_success() {
            return Err(PopError::InvalidStatus(status));
        }

        let text = res.text().await?;
        let host = {
            let doc = Document::from(text.as_str());
            extract_elastic_host(&doc).ok_or(PopError::MissingElasticHost)?
        };

        let url = {
            let mut url = Url::parse(&host)?;

            url.path_segments_mut()
                .map_err(|_| PopError::UrlCannotBeABase)?
                .extend(&["_search", "template"]);

            url
        };

        let body = json!({
            "params": {
                "site":"popcornflix",
                "query_string": query,
                "from": 0,
                "size": 70
            },
            "id":"moviesTemplate"
        });
        let res = self
            .client
            .post(url.as_str())
            .body(serde_json::to_string(&body)?)
            .basic_auth(url.username(), url.password())
            .header(ACCEPT, "application/json, text/plain, */*")
            .header(CONTENT_TYPE, "application/json")
            .header(ORIGIN, "https://www.popcornflix.com")
            .header("Sec-Fetch-Mode", "cors")
            .send()
            .await?;
        let status = res.status();

        if !status.is_success() {
            return Err(PopError::InvalidStatus(status));
        }

        let text = res.text().await?;

        Ok(serde_json::from_str(&text)?)
    }

    pub async fn get_info(&self, uid: &str) -> PopResult<serde_json::Value> {
        let url = {
            let mut url = Url::parse("https://api.unreel.me/v2/sites/popcornflix/movie")?;
            url.path_segments_mut()
                .map_err(|_| PopError::UrlCannotBeABase)?
                .push(uid);
            url
        };
        let res = self.client.get(url).send().await?;
        let status = res.status();
        if !status.is_success() {
            return Err(PopError::InvalidStatus(status));
        }

        let text = res.text().await?;

        Ok(serde_json::from_str(&text)?)
    }

    pub async fn get_stream_location(&self, uid: &str) -> PopResult<StreamLocation> {
        let url = {
            let mut url = Url::parse("https://api.unreel.me/v2/sites/popcornflix/videos")?;
            url.path_segments_mut()
                .map_err(|_| PopError::UrlCannotBeABase)?
                .extend(&[uid, "play-url"]);
            url
        };

        let res = self.client.get(url).send().await?;
        let status = res.status();
        if !status.is_success() {
            return Err(PopError::InvalidStatus(status));
        }

        let text = res.text().await?;

        Ok(serde_json::from_str(&text)?)
    }

    pub async fn get_stream(&self, loc: &StreamLocation) -> PopResult<MasterPlaylist> {
        let res = self.client.get(loc.url.as_str()).send().await?;
        let status = res.status();
        if !status.is_success() {
            return Err(PopError::InvalidStatus(status));
        }
        let text = res.text().await?;

        let playlist = match m3u8_rs::parse_playlist_res(text.as_bytes())? {
            Playlist::MasterPlaylist(playlist) => playlist,
            _ => return Err(PopError::InvalidPlaylistType),
        };

        Ok(playlist)
    }
}

impl Default for Client {
    fn default() -> Self {
        Client::new()
    }
}

fn extract_elastic_host(doc: &Document) -> Option<String> {
    use boa::syntax::ast::{
        constant::Const,
        node::Node,
    };

    doc.find(And(Name("script"), Not(Attr("src", ()))))
        .filter_map(|script| Some(script.find(Text).last()?.as_text()?.trim()))
        .filter(|text| text.starts_with("window"))
        .find_map(|text| {
            let mut lexer = boa::Lexer::new(text);
            lexer.lex().ok()?;
            let mut parser = boa::Parser::new(&lexer.tokens);
            let list = parser.parse_all().ok()?;
            let statements = list.statements();

            if statements.len() != 1 {
                return None;
            }

            let node = statements.first()?;

            match node {
                Node::Assign(assign) => match (assign.lhs(), assign.rhs()) {
                    (Node::GetConstField(field), Node::Const(Const::String(s))) => {
                        match field.obj() {
                            Node::Identifier(id) => {
                                if id.as_ref() == "window" && field.field() == "elasticHost" {
                                    Some(s.to_string())
                                } else {
                                    None
                                }
                            }
                            _ => None,
                        }
                    }
                    _ => None,
                },
                _ => None,
            }
        })
}

#[cfg(test)]
mod test {
    use super::*;

    fn assert_send<T: Send>(_: T) {}

    #[test]
    pub fn static_assertions() {
        let client = Client::new();
        let search = client.search("test");
        assert_send(search);
    }

    #[tokio::test]
    async fn it_works() {
        let client = Client::new();
        let search_results = client.search("test").await.unwrap();
        dbg!(&search_results);
        let first_hit = search_results.hits.hits.first().unwrap();
        dbg!(&first_hit);
        let info = client.get_info(&first_hit.source.uid).await.unwrap();
        dbg!(&info);
        let stream_location = client
            .get_stream_location(&first_hit.source.uid)
            .await
            .unwrap();
        dbg!(&stream_location);
        let playlist = client.get_stream(&stream_location).await.unwrap();
        dbg!(&playlist);

        let max = playlist
            .variants
            .iter()
            .max_by(|a, b| {
                a.bandwidth
                    .parse::<u32>()
                    .unwrap_or(0)
                    .cmp(&b.bandwidth.parse().unwrap_or(0))
            })
            .unwrap();

        dbg!(&max);
        dbg!(&max.uri);
        // Trimming the m3u8 appears to yield an mp4 url.
    }
}
