pub type PopResult<T> = Result<T, PopError>;
pub type M3u8ParseError<'a> = nom::IResult<&'a [u8], m3u8_rs::playlist::Playlist>;

#[derive(Debug)]
pub enum PopError {
    Reqwest(reqwest::Error),
    InvalidStatus(reqwest::StatusCode),
    Json(serde_json::Error),
    Url(url::ParseError),
    M3u8ParseError,

    MissingElasticHost,
    UrlCannotBeABase,
    InvalidPlaylistType,
}

impl From<reqwest::Error> for PopError {
    fn from(e: reqwest::Error) -> Self {
        PopError::Reqwest(e)
    }
}

impl From<serde_json::Error> for PopError {
    fn from(e: serde_json::Error) -> Self {
        PopError::Json(e)
    }
}

impl From<url::ParseError> for PopError {
    fn from(e: url::ParseError) -> Self {
        PopError::Url(e)
    }
}

impl From<M3u8ParseError<'_>> for PopError {
    fn from(_e: M3u8ParseError) -> Self {
        PopError::M3u8ParseError
    }
}
