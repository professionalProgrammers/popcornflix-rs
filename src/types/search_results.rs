use std::collections::HashMap;
use url::Url;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct SearchResults {
    pub took: u64,
    pub timed_out: bool,
    #[serde(rename = "_shards")]
    pub shards: Shards,
    pub hits: Hits,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Shards {
    pub total: u64,
    pub skipped: u64,
    pub successful: u64,
    pub failed: u64,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Hits {
    pub total: Total,
    pub max_score: Option<serde_json::Value>,
    pub hits: Vec<Hit>,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Total {
    pub value: u64,
    pub relation: String,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Hit {
    #[serde(rename = "_index")]
    pub index: String,

    #[serde(rename = "_type")]
    pub kind: String,

    #[serde(rename = "_id")]
    pub id: String,

    #[serde(rename = "_score")]
    pub score: f64,

    #[serde(rename = "_routing")]
    pub routing: String,

    #[serde(rename = "_source")]
    pub source: Source,

    pub sort: Vec<f64>,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Source {
    pub geoblocks: Option<Vec<String>>,
    pub sites: Vec<String>,

    #[serde(rename = "videoType")]
    pub video_type: String,

    pub channels: Vec<String>,
    pub metadata: Metadata,
    pub title: String,
    pub description: String,
    pub uid: String,
    pub status: String,
    pub rating: u64,
    pub cast: Vec<String>,
    pub creators: Option<Vec<String>>,
    pub directors: Vec<String>,
    pub genres: Vec<String>,
    pub mpaa: Option<String>,
    pub poster: Url,

    #[serde(rename = "mongodbCollectionName")]
    pub mongodb_collection_name: String,

    #[serde(rename = "lastSyncAt")]
    pub last_sync_at: String,

    #[serde(rename = "accessType")]
    pub access_type: String,

    pub unreel_join_type: String,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Metadata {
    #[serde(rename = "publishedDate")]
    pub published_date: String,
    pub tags: Vec<String>,
    pub thumbnails: Option<Thumbnails>,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Thumbnails {
    pub medium: Url,
    pub maxres: Option<Url>,
    pub high: Option<Url>,
    pub standard: Option<Url>,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}
