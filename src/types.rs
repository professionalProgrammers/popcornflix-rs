pub mod search_results;
pub use self::search_results::SearchResults;
use std::collections::HashMap;
use url::Url;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct StreamLocation {
    pub url: Url,

    #[serde(rename = "isSigned")]
    pub is_signed: bool,

    #[serde(rename = "type")]
    pub data_type: String,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}
